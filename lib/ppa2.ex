defmodule Ppa2 do
  alias Ppa2.Bmi
  alias Ppa2.Retirement
  alias Ppa2.Controller.DistanceFormula
  alias Ppa2.Controller.SplitController

  def main(_args) do
    IO.puts("\nSelect from the following functions:")
    selection = IO.gets("1) BMI calculator\n2) Retirement savings calculator\n3) Shortest distance calculator\n4) Tip splitting calculator\nq: quit\n")

    case Integer.parse(selection) do
      :error ->
        parse_selection(selection)
      {number, _} ->
        parse_selection(number)
    end
  end

  def bmi_prompt do
    ft = IO.gets("Feet: ")
    inch = IO.gets("Inches: ")
    weight = IO.gets("Weight in pounds: ")
    IO.puts("\n")

    IO.puts(Bmi.execute(ft, inch, weight))
  end

  def distance_prompt do
    IO.puts("\nPreviously computed distances:")
    print_all_dists(DistanceFormula.get_all_results())
    IO.puts("\n")

    IO.puts("Please provide two points in the format (x1, y1) and (x2, y2). We will prompt you for each value individually.")

    x1 = IO.gets("x1: ") |> String.slice(0..-2)
    y1 = IO.gets("y1: ") |> String.slice(0..-2)
    x2 = IO.gets("x2: ") |> String.slice(0..-2)
    y2 = IO.gets("y2: ") |> String.slice(0..-2)

    result = DistanceFormula.add_new_result(x1, y1, x2, y2)

    IO.puts(result)
  end

  def print_all_dists(dists) do
    dists
    |> Enum.each(fn dist -> IO.puts("x1: #{dist.x1} y1: #{dist.y1} x2: #{dist.x2} y2: #{dist.y2} result: #{dist.result} date: #{Time.to_string(dist.date)}, #{Date.to_string(dist.date)}") end)
  end

  def print_all_splits(splits) do
    splits
    |> Enum.each(fn split -> IO.puts("diners: #{split.diners}, total: #{split.total}, result: #{split.result} date: #{Time.to_string(split.date)}, #{Date.to_string(split.date)}") end)
  end

  def retirement_prompt do
    age = IO.gets("Age: ")
    salary = IO.gets("Annual salary (in dollars): ")
    percent_saved = IO.gets("Percent of annual salary saved: ")
    goal = IO.gets("Savings goal (in dollars): ")

    IO.puts(Retirement.execute(age, salary, percent_saved, goal))
  end

  def split_prompt do
    IO.puts("\nPreviously computed splits:")
    print_all_splits(SplitController.get_all_results())
    IO.puts("\n")
    total = IO.gets("Total amount for meal without dollar sign (e.g. 30.27): ") |> String.slice(0..-2)
    number_of_diners = IO.gets("Number of diners splitting the tab: ") |> String.slice(0..-2)

    result = SplitController.add_new_result(number_of_diners, total)

    IO.puts(result)
  end

  def parse_selection(selection) when is_integer(selection) do
    # run corresponding function

    case selection do
      1 -> Ppa2.bmi_prompt
      2 -> Ppa2.retirement_prompt
      3 -> Ppa2.distance_prompt
      4 -> Ppa2.split_prompt
      _ -> "Please provide an actual number."
    end

    main(:noop)
  end

  def parse_selection("q\n") do
    IO.puts "Good bye"
  end

  def parse_selection(_) do
    IO.puts("Please provide an actual number.")
    main(:noop)
  end
end
