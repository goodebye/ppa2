defmodule Ppa2.Endpoint do
  use Plug.Router
  alias Ppa2.Controller.DistanceFormula
  alias Ppa2.Controller.SplitController

  plug(Plug.Logger)
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)
  plug(:dispatch)

  post "/distance" do
    # compute new distance based on input
    {status, body} =
      case conn.body_params do
        %{"x1" => x1, "x2" => x2, "y1" => y1, "y2" => y2} -> { 200, Poison.encode!(%{"result" => DistanceFormula.add_new_result(x1, y1, x2, y2)})}
        _ -> {404, Poison.encode!(%{"error" => "error lol"})}
      end

    send_resp(conn, status, body)
  end

  get "/distance" do
    # get all results for distance calculations
    send_resp(conn, 200, Poison.encode!(DistanceFormula.get_all_results()))
  end

  post "/split" do
    # compute new distance based on input
    {status, body} =
      case conn.body_params do
        %{ "diners" => diners, "total" => total} -> { 200, Poison.encode!(%{"result" => SplitController.add_new_result(diners, total)})}
        _ -> {404, Poison.encode!(%{"error" => "error lol"})}
      end

    send_resp(conn, status, body)
  end

  get "/split" do
    send_resp(conn, 200, Poison.encode!(SplitController.get_all_results()))
  end
end
