defmodule Ppa2.Bmi do
  defstruct height: 0, weight: 0
  def execute(ft, inch, weight) do
    stats = stats_to_floats([ft, inch, weight])

    case length(stats) do
      3 ->
        # keep going
        [ft, inch, weight] = stats
        calculate(%Ppa2.Bmi{weight: weight, height: ft * 12 + inch})

      _ ->
        "Incorrect input"
    end
  end

  defp calculate(stats = %Ppa2.Bmi{}) do
    stats
    |> pounds_to_kg
    |> in_to_cm
    |> convert_bmi
    |> Float.round(1)
  end

  defp pounds_to_kg(stats = %Ppa2.Bmi{weight: weight}) do
    %{stats | weight: weight * 0.45}
  end

  def in_to_cm(stats = %Ppa2.Bmi{height: height}) do
    %{stats | height: height * 0.025}
  end

  defp convert_bmi(%Ppa2.Bmi{height: height, weight: weight}) do
    weight / (height * height)
  end

  defp stats_to_floats(stats_list) do
    stats_list
    |> Enum.filter(fn stat -> not is_nil(stat) end)
    |> Enum.map(fn stat -> Float.parse(stat) end)
    |> Enum.map(fn stat ->
      case stat do
        {number, _} when number > 0 -> number
        _ -> :error
      end
    end)
    |> Enum.filter(fn stat -> (stat != :error) end)
  end
end
