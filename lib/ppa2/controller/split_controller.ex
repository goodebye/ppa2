defmodule Ppa2.Controller.SplitController do
  alias Ppa2.DB.Split
  alias Ppa2.Split, as: Splitter

  def add_new_result(people, total) do
    result = Splitter.execute(total, people)

    Ppa2.Repo.insert(%Split{diners: people, total: total, result: result})

    result
  end

  def get_all_results() do
    Ppa2.Repo.all(Split)
    |> Enum.map(fn row -> %{ diners: row.diners, total: row.total, result: row.result, date: row.inserted_at } end)
  end
end
