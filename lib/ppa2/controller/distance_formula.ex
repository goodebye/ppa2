defmodule Ppa2.Controller.DistanceFormula do
  alias Ppa2.DB.DistanceFormula
  alias Ppa2.Distance

  def add_new_result(x1, y1, x2, y2) do
    result = Distance.execute(x1, y1, x2, y2)

    Ppa2.Repo.insert(%DistanceFormula{x1: x1, x2: x2, y1: y1, y2: y2, result: Kernel.inspect(result)})

    result
  end

  def get_all_results() do
    Ppa2.Repo.all(DistanceFormula)
    |> Enum.map(fn row -> %{ x1: row.x1, x2: row.x2, y1: row.y1, y2: row.y2, result: row.result, date: row.inserted_at } end)
  end
end
