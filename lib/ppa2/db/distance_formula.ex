defmodule Ppa2.DB.DistanceFormula do
  use Ecto.Schema

  schema "distance_formula" do
    field :x1, :string
    field :x2, :string
    field :y1, :string
    field :y2, :string
    field :result, :string

    timestamps()
  end
end
