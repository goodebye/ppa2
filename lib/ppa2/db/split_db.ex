defmodule Ppa2.DB.Split do
  use Ecto.Schema

  schema "split" do
    field :diners, :string
    field :total, :string
    field :result, :string

    timestamps()
  end
end

