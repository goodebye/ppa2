defmodule Ppa2.Distance do
  def execute(x1, y1, x2, y2) do
    input_to_points([[x: x1, y: y1],  [x: x2, y: y2]])
  end

  def input_to_points(points) do
    case parse_points(points) do
      {:ok, point1, point2} ->
        :math.pow(point2[:x] - point1[:x], 2) + :math.pow(point2[:y] - point1[:y], 2)
        |> :math.sqrt()
        |> Float.round(5)

      _ -> "Incorrect input"
    end
  end

  defp parse_points(points) do
    points
    |> Enum.reduce_while({:ok}, fn point, acc ->
      case parse_point(point) do
        {:ok, parsed_point} -> {:cont, Tuple.append(acc, parsed_point)}
        {:error} -> {:halt, :error}
      end
    end)
  end

  defp parse_point(point) do
    with true <- (not is_nil(point[:x]) and not is_nil(point[:y])),
         {x, _} <- Float.parse(point[:x]),
         {y, _} <- Float.parse(point[:y])
    do
      {:ok, [x: x, y: y]}
    else
      _ -> {:error}
    end
  end
end
