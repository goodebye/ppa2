defmodule Ppa2.Split do
  def execute(total, diners) do
    case input_to_numbers(total, diners) do
      {parsed_total, parsed_diners} ->
        [total: parsed_total, diners: parsed_diners]
        |> add_gratuity
        |> evenly_divide
        |> balance_portions
        |> format_output

      _ -> "Invalid input"
    end
  end

  defp input_to_numbers(total, diners) do
    with {parsed_total, _} when not is_nil(total) <- Float.parse(total),
         {parsed_diners, _} when not is_nil(diners) <- Integer.parse(diners)
    do
      {Float.round(parsed_total, 2), parsed_diners}
    else
      _ -> :error
    end
  end

  defp format_output(portions_list) do
    portions_list
    |> Enum.with_index
    |> Enum.reduce("", fn({portion, index}, final_string) ->
      final_string <> "Guest #{index + 1}: $#{:erlang.float_to_binary(portion, [decimals: 2])}, "
    end)
    |> String.slice(0..-3)
  end

  defp add_gratuity([total: total, diners: diners]) do
    [total: Float.round(total * 1.15, 2), diners: diners]
  end

  defp evenly_divide([total: total, diners: diners]) do
    [total: total, diners: diners, portion: total / diners]
  end

  defp balance_portions([total: total, diners: diners, portion: portion]) do
    total_in_cents = trunc(total * 100)
    portion_in_cents = trunc(portion * 100)

    cents_remaining = total_in_cents - (portion_in_cents * diners)

    Enum.reduce(0..diners - 1, [], fn(diner_number, diners_portions) ->
      if diner_number < cents_remaining do
        diners_portions ++ [Float.round((portion_in_cents + 1) / 100, 2)]
      else
        diners_portions ++ [Float.round(portion_in_cents / 100, 2)]
      end
    end)
  end
end
