defmodule Ppa2.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Ppa2.Worker.start_link(arg)
      # {Ppa2.Worker, arg}
      {Ppa2.Repo, []},
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Ppa2.Endpoint,
        options: [port: 5000]
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ppa2.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
