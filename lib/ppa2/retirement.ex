defmodule Ppa2.Retirement do
  def execute(age, salary, percent_saved, goal) do
    case input_to_floats([age: age, salary: salary, percent_saved: percent_saved, goal: goal]) do
      :error -> "Incorrect input"
      stats -> stats
               |> calculate
               |> Float.ceil
               |> trunc
               |> (&(if &1 > 100, do: "sorry you're dead bro", else: "You will be #{&1} years old when you reach this amount saved.")).()
    end
  end

  defp calculate(stats) do
    stats[:goal] / (stats[:salary] * stats[:percent_saved] / 100 * 1.35) + stats[:age] # == age reached goal
  end

  defp input_to_floats(retirement_stats) do
    retirement_stats
    |> Enum.reduce_while([], fn {key, val}, acc ->
      if not is_nil(val) do
        case Float.parse(val) do
          {parsed_val, _} when parsed_val > 0 -> {:cont, acc ++ [{key, parsed_val}]}
          _ -> {:halt, :error}
        end
      else
        {:halt, :error}
      end
    end)
  end
end
