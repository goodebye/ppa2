defmodule Ppa2.Repo do
  use Ecto.Repo,
    otp_app: :ppa2,
    adapter: Ecto.Adapters.Postgres
end
