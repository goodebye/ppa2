defmodule Ppa2.ControllerTest do
  use ExUnit.Case, async: false
  alias Ppa2.Controller.SplitController
  alias Ppa2.Controller.DistanceFormula, as: DistanceController
  alias Ppa2.DB.Split
  alias Ppa2.DB.DistanceFormula, as: Distance
  alias Ppa2.Split, as: Splitter
  alias Ppa2.Distance, as: Distancer

  import ExMock

  @tag :db
  test "distance controller should query the database on get_all_results" do
    with_mock Ppa2.Repo, [all: fn _ -> [] end] do
      DistanceController.get_all_results()

      assert called Ppa2.Repo.all(Distance)
    end
  end

  @tag :db
  test "split controller should query the database on get_all_results" do
    with_mock Ppa2.Repo, [all: fn _ -> [] end] do
      SplitController.get_all_results()

      assert called Ppa2.Repo.all(Split)
    end
  end

end
