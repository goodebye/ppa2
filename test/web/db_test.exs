defmodule Ppa2.DBtest do
  use ExUnit.Case, async: false
  alias Ppa2.Controller.SplitController
  alias Ppa2.Controller.DistanceFormula, as: DistanceController
  alias Ppa2.DB.Split
  alias Ppa2.DB.DistanceFormula, as: Distance
  alias Ppa2.Split, as: Splitter
  alias Ppa2.Distance, as: Distancer

  import ExMock

  @tag :db
  test "split controller should attempt to insert into database with correct values" do
    # this uses the ExMock library to create a mock (Ppa2.Repo.insert) and a stub (Split.execute)
    with_mocks( [{Ppa2.Repo, [], [insert: fn _ -> :ok end]}, {Splitter, [], [execute: fn _, _ -> "0" end]}]) do
      SplitController.add_new_result("23", "2")

      assert called Ppa2.Repo.insert(%Split{diners: "23", total: "2", result: IO.inspect("0")})
    end
  end


  @tag :db
  test "distance controller should attempt to insert into database with correct values" do
    # this uses the ExMock library to create a mock (Ppa2.Repo.insert) and a stub (Distance.execute)
    with_mocks( [{Ppa2.Repo, [], [insert: fn _ -> :ok end]}, {Distancer, [], [execute: fn _, _, _, _ -> "0" end]}]) do
      DistanceController.add_new_result("0", "0", "3", "4")

      assert called Ppa2.Repo.insert(%Distance{x1: "0", y1: "0", x2: "3", y2: "4", result: Kernel.inspect("0")})
    end
  end
end
