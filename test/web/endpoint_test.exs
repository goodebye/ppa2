defmodule Ppa2.EndpointTest do
  use ExUnit.Case, async: false
  use Plug.Test

  @opts Ppa2.Endpoint.init([])

  test "it returns 200 for GET /distance" do
    conn = conn(:get, "/distance", %{})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.status == 200
  end

  test "it returns 200 for GET /split" do
    conn = conn(:get, "/split", %{})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.status == 200
  end


  test "POST /distance returns 5.0 for standard 3-4-5 triangle input" do
    conn = conn(:post, "/distance", %{"x1" => "0", "y1" => "0", "x2" => "3", "y2" => "4"})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.resp_body == "{\"result\":5.0}"
  end

  test "POST /split returns 5.75 each for split of 10.00 btwn 2 people" do
    conn = conn(:post, "/split", %{"diners" => "2", "total" => "10"})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.resp_body == "{\"result\":\"Guest 1: $5.75, Guest 2: $5.75\"}"
  end

  test "POST /split fails with invalid input" do
    conn = conn(:post, "/split", %{"this" => "is invalid"})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.resp_body =="{\"error\":\"error lol\"}"
  end

  test "POST /distance fails with invalid input" do
    conn = conn(:post, "/distance", %{"this" => "is invalid"})

    conn = Ppa2.Endpoint.call(conn, @opts)

    assert conn.resp_body =="{\"error\":\"error lol\"}"
  end
end
