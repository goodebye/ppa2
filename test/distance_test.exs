defmodule DistanceTest do
  use ExUnit.Case, async: false
  alias Ppa2.Distance

  # convert nil to string for video screencast to fix this
  test "fails on non-numerical input" do
    assert Distance.execute("hello", "world", "420", "69") == "Incorrect input"
  end

  test "returns a number given numerical input" do
    assert is_number(Distance.execute("30", "25", "50", "35"))
  end

  test "returns correct distance given two valid points" do
    assert inspect(Distance.execute("10", "10", "10", "15")) == "5.0"
  end

  test "returns a number with at most 5 decimal places" do
    assert inspect(Distance.execute("10", "10", "12", "13")) == "3.60555"
  end

  test "fails on empty input" do
    #also good for screencast
    assert Distance.execute(nil, nil, nil, nil) == "Incorrect input"
  end
end
