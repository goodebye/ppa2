defmodule RetirementTest do
  use ExUnit.Case, async: false
  alias Ppa2.Retirement

  test "should fail on invalid (empty) input" do
    assert Retirement.execute("", "", "", "") == "Incorrect input"
  end

  test "should return a string" do
    assert is_bitstring(Retirement.execute("10", "10", "10", "10"))
  end

  test "fails on non-numerical input" do
    assert Retirement.execute("not", "correct", "input", "lol") == "Incorrect input"
  end

  test "should fail on negative value input (one or more)" do
    assert Retirement.execute("235", "-12", "2353", "-15") == "Incorrect input"
  end

  test "should fail on nil input" do
    assert Retirement.execute("235", nil, "235", "23523") == "Incorrect input"
  end

  test "should return 18 y/o for a 10 year old with 10,000 salary saving at 10% per year with a goal of 10,000" do
    assert Retirement.execute("10", "10000", "10", "10000") == "You will be 18 years old when you reach this amount saved."
  end
end
