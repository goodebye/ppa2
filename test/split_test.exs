defmodule SplitTest do
  use ExUnit.Case, async: false
  alias Ppa2.Split

  test "a tab of $10.00 split btwn 2 people results in $5.75 each" do
    assert Split.execute("10.00", "2") == "Guest 1: $5.75, Guest 2: $5.75"
  end

  test "a tab of $11.00 split btwn 2 people results in 6.33 for one and 6.32 for the other" do
    assert Split.execute("11.00", "2") == "Guest 1: $6.33, Guest 2: $6.32"
  end

  test "a tab of $9 split btwn 10 people results in half paying $1.04 and half paying $1.03" do
    assert Split.execute("9.00", "10") == "Guest 1: $1.04, Guest 2: $1.04, Guest 3: $1.04, Guest 4: $1.04, Guest 5: $1.04, Guest 6: $1.03, Guest 7: $1.03, Guest 8: $1.03, Guest 9: $1.03, Guest 10: $1.03"
  end

  test "fails on non-numerical input" do
    assert Split.execute("non", "numerical") == "Invalid input"
  end

  test "fails on empty inputs" do
    assert Split.execute("", "")
  end

  test "returns a string for correct numerical input" do
    assert is_bitstring(Split.execute("10", "10"))
  end

  test "fails on any nil input" do
    assert Split.execute("nil", nil) == "Invalid input"
  end
end
