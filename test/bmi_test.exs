defmodule BmiTest do
  use ExUnit.Case, async: false
  alias Ppa2.Bmi

  test "fails on empty input" do
    #also good for screencast
    assert Bmi.execute(nil, nil, nil) == "Incorrect input"
  end


  test "fails on any non-number input" do
    assert Bmi.execute("30.0", "50", "fail") == "Incorrect input"
  end

  test "returns a number when given numerical inputs" do
    assert is_number(Bmi.execute("5", "10", "180"))
  end

  test "returns 22.7 for person of height 5 foot 3 inches and weight 125 lbs" do
    assert Bmi.execute("5", "3", "125") == 22.7
  end

  test "fails on negative values for height/weight" do
    # this one would be good as example live coding thing
    assert Bmi.execute("-5", "3", "125") == "Incorrect input"
  end
end
