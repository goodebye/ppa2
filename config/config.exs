use Mix.Config

config :ppa2, Ppa2.Repo,
  database: "ppa2_repo",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_USER") || "password",
  hostname: System.get_env("POSTGRES_HOST") || "localhost"

config :ppa2, ecto_repos: [Ppa2.Repo]

config :logger, level: :info
