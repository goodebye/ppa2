# Ppa2

## Installation/Setup

I did this assignment on Windows, which is relatively easy to get set up.

    Install latest Elixir version (as of this writing, 1.9.1) using the installer you can download here. Simply follow the on-screen prompts.
    Clone this repo, cd into it.
    Run mix escript.build to compile the app.
    Run escript ppa1 to run the app.
    Alternately, you can run iex -S mix to open an interactive shell to play with things

## Tests

- unit tests: `mix test`
- integration tests: `mix test --only db`

## Screencasts

see `./screencasts`

## API

GET /distance

returns previously computed distances

POST /distance

provide JSON object like { "x1": "0", "x2": "3", "y1": "0", "y2": "4" } with application/json as the header type
returns resulting computation

GET /split

returns previously computed splits

POST /distance

provide JSON object like { "diners": "14", "total": "100" } with application/json as the header type
returns resulting computation