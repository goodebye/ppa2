defmodule Ppa2.Repo.Migrations.CreateDistanceFormula do
  use Ecto.Migration

  def change do
    create table(:distance_formula) do
      add :x1, :string
      add :x2, :string
      add :y1, :string
      add :y2, :string
      add :result, :string
    end
  end
end
