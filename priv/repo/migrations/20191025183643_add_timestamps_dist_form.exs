defmodule Ppa2.Repo.Migrations.AddTimestampsDistForm do
  use Ecto.Migration

  def change do
    alter table(:distance_formula) do
      timestamps()
    end
  end
end
