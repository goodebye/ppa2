defmodule Ppa2.Repo.Migrations.CreateSplitTable do
  use Ecto.Migration

  def change do
    create table(:split) do
      add :diners, :string
      add :total, :string
      add :result, :string

      timestamps()
    end
  end
end
